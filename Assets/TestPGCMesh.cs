﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TestPGCMesh : MonoBehaviour {
	Mesh mesh;
	List<Vector3> vertices = new List<Vector3> ();
	List<int> triangles = new List<int> ();
	int triCount = 0;
	// Use this for initialization
	void Start () {
		mesh = GetComponent<MeshFilter> ().mesh;

		BuildCube (0, 0, 0);
	}

	void BuildFace(int x1, int y1, int z1, int x2, int y2, int z2, int x3, int y3, int z3, int x4, int y4, int z4)
	{
		vertices.Add (new Vector3 (x1, y1, z1));
		vertices.Add (new Vector3 (x2, y2, z2));
		vertices.Add (new Vector3 (x3, y3, z3));
		vertices.Add (new Vector3 (x4, y4, z4));

		// first tri
		triangles.Add (0 + (triCount * 3));
		triangles.Add (2 + (triCount * 3));
		triangles.Add (1 + (triCount * 3));
		triangles.Add (2 + (triCount * 3));
		triangles.Add (3 + (triCount * 3));
		triangles.Add (1 + (triCount * 3));
		triCount++;
		
	}

	void BuildCube(float x, float y, float z)
	{


		BuildFace (-1, 0, -1, 1, 0, -1, -1, 0, 1, 1, 0, 1);

		BuildFace (-1, 1, -1, 1, 1, -1, -1, 1, 1, 1, 1, 1);


		// Face in the positive Z
		mesh.vertices = vertices.ToArray ();
		mesh.triangles = triangles.ToArray ();
		mesh.Optimize ();
		mesh.RecalculateNormals();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

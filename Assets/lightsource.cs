﻿using UnityEngine;
using System.Collections;

public class lightsource : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	float timer = 0;
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if (timer > 0.5f)
		{
			timer = 0;
			Debug.Log("Luce");
			RaycastHit hit;
			Ray ray = new Ray( GameObject.FindGameObjectWithTag("Player").transform.position, -GameObject.FindGameObjectWithTag("Player").transform.up); //Camera.mainCamera.ScreenPointToRay(new Vector3(Screen.width / 2.0f, Screen.height / 2.0f, 0));
			if (Physics.Raycast(ray, out hit, 4.0f))
			{
				WorldData wdata = GameObject.Find ("World").GetComponent<WorldGameObject> ().WorldData;
				
				wdata.SetBlockLightWithRegeneration((int) hit.point.x, (int) hit.point.z, (int) hit.point.y, 255);
				GameObject.Find("World").GetComponent<WorldGameObject>().pWorld.RegenerateChunks();

			}
		}
	}
}

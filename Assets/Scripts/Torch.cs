﻿using UnityEngine;
using System.Collections;

public class Torch : MonoBehaviour {

	World mWorld;
	WorldData mWorldData;
	GameObject w;
	WorldGameObject mWorldGO;

	bool needUpdate = true;

	// Use this for initialization
	void Start () {
		w = GameObject.Find ("World");
		mWorldGO = w.GetComponent<WorldGameObject>();
		mWorld = mWorldGO.pWorld;
		mWorldData = mWorldGO.WorldData;
	}
	
	// Update is called once per frame
	void Update () {

		if (!needUpdate)
			return;

		if (Input.GetKeyDown(KeyCode.F))
		{
			Destroy(gameObject);
		}

	}
}

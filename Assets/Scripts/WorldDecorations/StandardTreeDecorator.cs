﻿using UnityEngine;
using System.Collections.Generic;

public static class DecorationParser
{

	public static void ParseDecoration(string filename, WorldData w, IntVect origin)
	{
		//TextAsset t = Resources.Load ("Decorations/" + filename) as TextAsset;

		string path = w.appPath + "/Resources/Decorations/" + filename + ".txt";
		string[] first_parse = System.IO.File.ReadAllLines (path);
		List<string[]> second_parse = new List<string[]> ();
		for(int i = 0; i < first_parse.Length; i++)
		{
		//	first_parse[i] = first_parse[i].Replace("Cube:", "");
			second_parse.Add(first_parse[i].Split( new char[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries ));	
		}

		for(int i = 0; i < second_parse.Count; i++)
		{
			IntVect offset = new IntVect( System.Convert.ToInt32( second_parse[i][0] ), System.Convert.ToInt32( second_parse[i][1] ), System.Convert.ToInt32( second_parse[i][2] ) );
			BlockType t = (BlockType)System.Convert.ToInt32( second_parse[i][3]);
			//Debug.Log(second_parse[i][3]);


			w.SetBlockType( origin.X + offset.X, origin.Y + offset.Y, origin.Z + offset.Z, t);
		}


		//return rValue;
	}


}

public class TestDecorator : IDecoration
{
	private readonly WorldData m_WorldData;
	private System.Random rand;
	BlockType[,,] structure;
	public TestDecorator(WorldData worldData)
	{
		m_WorldData = worldData;
		rand = new System.Random ();
	}
	
	
	public void Decorate(int blockX, int blockY, int blockZ, Random random)
	{
		//m_WorldData.SetBlockType (blockX, blockY, blockZ, BlockType.Test);
		if (random.RandomRange(1, 100) > 90)
		{
			DecorationParser.ParseDecoration ("test", m_WorldData, new IntVect (blockX, blockY, blockZ));
				
			//GameObject o = 
			//GameObject.Find("World").GetComponent<WorldGameObject>().InstantiateObject("alberoquadrato", new Vector3( blockX * m_WorldData.WidthInBlocks, blockY * m_WorldData.HeightInBlocks, blockZ * m_WorldData.DepthInBlocks));
			//Debug.Log("Building at... " + blockX+ "," + blockY + ","+blockZ);
			//for(int z = blockZ; z < blockZ+10; z++)
			//{
			//	m_WorldData.SetBlockType(blockX, blockY, z, BlockType.Lava);
			//}
		}
	}
}

public class BushDecorator : StandardTreeDecorator, IDecoration
{
	private readonly WorldData m_WorldData;

	public BushDecorator(WorldData worldData) : base(worldData)
	{
		m_WorldData = worldData;
	}

	public void Decorate(int blockX, int blockY, int blockZ, Random random)
	{
		// 0.5% chance for a bush
		if (random.RandomRange(1,1000) < 999)
		{
			return;
		}

		CreateSphereAt (blockX, blockY, blockZ, 3);
	}
	
}

public class StandardTreeDecorator : IDecoration
{
    private readonly WorldData m_WorldData;

    public StandardTreeDecorator(WorldData worldData)
    {
        m_WorldData = worldData;
    }


    public void Decorate(int blockX, int blockY, int blockZ, Random random)
    {
        if (IsAValidLocationforDecoration(blockX, blockY, blockZ, random))
        {
           CreateDecorationAt(blockX, blockY, blockZ, random);
        }
    }

    /// <summary>
    /// Determines if a tree decoration even wants to be at this location.
    /// </summary>
    /// <param name="blockX"></param>
    /// <param name="blockY"></param>
    /// <param name="blockZ"></param>
    /// <param name="random"></param>
    /// <returns></returns>
    protected bool IsAValidLocationforDecoration(int blockX, int blockY, int blockZ, Random random)
    {
        // We don't want TOO many trees...make it a 1% chance to be drawn there.
        if (random.RandomRange(1, 100) < 99)
        {
            return false;
        }

        // Trees don't like to grow too high
        if (blockZ >= m_WorldData.DepthInBlocks - 20)
        {
            return false;
        }

        // Trees like to have a minimum amount of space to grow in.
        return SpaceAboveIsEmpty(blockX, blockY, blockZ, 8, 2, 2);
    }

    protected void CreateDecorationAt(int blockX, int blockY, int blockZ, Random random)
    {
        int trunkLength = random.RandomRange(10, 16);
        // Trunk
        for (int z = blockZ + 1; z <= blockZ + trunkLength; z++)
        {
            CreateTrunkAt(blockX, blockY, z);
        }

        // Leaves
        CreateSphereAt(blockX, blockY, blockZ + trunkLength, random.RandomRange(3, 4));
    }

    /// <summary>
    /// Creates the tree canopy...a ball of leaves.
    /// </summary>
    /// <param name="blockX"></param>
    /// <param name="blockY"></param>
    /// <param name="blockZ"></param>
    /// <param name="radius"></param>
    protected void CreateSphereAt(int blockX, int blockY, int blockZ, int radius)
    {
        for (int x = blockX - radius; x <= blockX + radius; x++)
        {
            for (int y = blockY - radius; y <= blockY + radius; y++)
            {
                for (int z = blockZ - radius; z <= blockZ + radius; z++)
                {
                    if (Vector3.Distance(new Vector3(blockX, blockY, blockZ), new Vector3(x, y, z)) <= radius)
                    {
                        m_WorldData.SetBlockType(x, y, z, BlockType.Leaves);
                    }
                }
            }
        }
    }

    private void CreateTrunkAt(int blockX, int blockY, int z)
    {
        m_WorldData.SetBlockType(blockX, blockY, z, BlockType.Dirt);
    }

    protected bool SpaceAboveIsEmpty(int blockX, int blockY, int blockZ, int depthAbove, int widthAround, int heightAround)
    {
        for (int z = blockZ + 1; z <= blockZ + depthAbove; z++)
        {
            for (int x = blockX - widthAround; x <= blockX + widthAround; x++)
            {
                for (int y = blockY - heightAround; y < blockY + heightAround; y++)
                {
                    if (m_WorldData.GetBlock(x, y, z).Type != BlockType.Air)
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public override string ToString()
    {
        return "Standard Tree";
    }
}
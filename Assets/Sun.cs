﻿using UnityEngine;
using System.Collections;

public class Sun : MonoBehaviour {

	public float DayDuration = 80;
	public float NightDuration = 80;

	float CycleTimer = 0;

	bool day = true;

	// Use this for initialization
	void Start () {
		Debug.Log ("E' giorno...");
		//iTween.RotateTo (gameObject, iTween.Hash ("rotation", new Vector3 (0,-360, -360), "time", 30, "easeType", "Linear"));
	}
	
	// Update is called once per frame
	void Update () {
		if (day)
		{			
			CycleTimer += Time.deltaTime;
			float cyclePosition = 1 - (CycleTimer / DayDuration);
			Color c = RenderSettings.ambientLight;
			RenderSettings.ambientLight = new Color(  cyclePosition, cyclePosition, cyclePosition );
			if (CycleTimer > DayDuration)
			{
				CycleTimer = 0;
				day = false;
				Debug.Log ("E' notte...");
			}

		}
		else
		{
			CycleTimer += Time.deltaTime;
			float cyclePosition = (CycleTimer / NightDuration);
			Color c = RenderSettings.ambientLight;
			RenderSettings.ambientLight = new Color(  cyclePosition, cyclePosition, cyclePosition );
			if (CycleTimer > DayDuration)
			{
				CycleTimer = 0;
				day = true;
				Debug.Log ("E' giorno...");
			}
			
		}

	}
}
